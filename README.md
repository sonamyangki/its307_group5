# its307_group5
# Abstract
The Project titled 'GOLD PRICE PREDICTION' predicts the gold EFT price based on the
previous year’s gold price data. The main goal of this project is to forecast the rise and fall in
the daily gold rates that can help investors to decide when to buy or sell the gold. The price of
gold is calculated by looking at the dataset that contains the previous year’s gold price.
Although there are a number of studies that analyze the correlation between the gold price
and certain economic variables. We have applied machine learning technique to predict
financial variables and we have focused on predicting the gold price ETF using a KNN
regressor algorithm as our dataset is a numerical dataset. 


# Link to gold price prediction page
machinelearning.pythonanywhere.com

# Members
1. Sonam Deki (12200080)
2. Sonam Tshering (12200082)
3. Sonam Yangki (12200085)