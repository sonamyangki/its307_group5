from flask import Flask, request, render_template
import pickle
import numpy as np
import pandas as pd


app = Flask(__name__)
model = pickle.load(open('model.pkl', 'rb'))

@app.route('/')
def home():
    return render_template('homepage.html')

@app.route('/predict', methods=['GET','POST'])
def predict():
    if request.method == 'POST':
        spx = request.form.get('spx')
        uso = request.form.get('uso')
        slv = request.form.get('slv')
        eurusd = request.form.get('eur/usd')
        year = request.form.get('year')
        month = request.form.get('month')

        sampl = np.array([[spx,uso,slv,eurusd,year,month]])

        sample = pd.DataFrame(sampl, columns = (['SPX','USO','SLV','EUR/USD','Year','Month']))
        prediction = model.predict(sample)
        #Round the output to 2 decimal places
        output = round(prediction[0], 2)

        return render_template('index.html', prediction_text = 'Predicted Price of the GOLD is: ${}'.format(output))

    else:
        return render_template('index.html')


if __name__ == "__main__":
    app.run(debug=True)